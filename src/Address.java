
public class Address implements Comparable<Address>{
    private String city;
    private int postCode;

    public Address() {

    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getPostCode() {
        return postCode;
    }

    public void setPostCode(int postCode) {
        this.postCode = postCode;
    }
    
    @Override
    public int compareTo(Address address) {
        return this.city.compareTo(address.getCity());
    }

    @Override
    public String toString() {
        return "Address [city=" + city + ", postCode=" + postCode + "]";
    }

    public static class Builder {
        private String city;
        private int postCode;

        public Builder with() {
            return this;
        }

        public Builder city(String city) {
            this.city = city;
            return this;
        }

        public Builder postCode(int postCode) {
            this.postCode = postCode;
            return this;
        }

        public Address build() {
            Address address = new Address();
            address.setCity(city);
            address.setPostCode(postCode);
            return address;
        }
    }

}
