import java.util.Comparator;

public class Person implements Comparable<Person> {

    private String name;
    private int age;
    private Address address;

    public Person() {

    }

    public Person(String name, int age, Address address) {
        super();
        this.name = name;
        this.age = age;
        this.address = address;
    }

    @Override
    public int compareTo(Person person) {
        if (this.age > person.age)
            return 1;
        else if (this.age < person.age)
            return -1;
        return 0;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Person [name=" + name + ", age=" + age + ", address=" + address + "]";
    }

    public final static Comparator<Person> comparatorByName = new Comparator<Person>() {

        @Override
        public int compare(Person person1, Person person2) {
            return person2.getName().compareTo(person1.getName());
        }

    };
    
    public final static Comparator<Person> comparatorByCityAddress = new Comparator<Person>() {

        @Override
        public int compare(Person person1, Person person2) {
            return person1.getAddress().compareTo(person2.getAddress());
        }

    };

    public static class Builder {
        private int age;
        private String name;
        private Address address;

        public Builder with() {
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder age(int age) {
            this.age = age;
            return this;
        }

        public Builder address(Address address) {
            this.address = address;
            return this;
        }

        public Person build() {
            Person person = new Person();
            person.setAge(age);
            person.setName(name);
            person.setAddress(address);
            return person;
        }
    }

}
