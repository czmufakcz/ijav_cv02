import java.io.ObjectInputStream.GetField;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Stack;

public class Main {

    public static void main(String[] args) {
        Main.task1();
        Main.task2();
        Main.task3();
        Main.task4();

    }

    public static void task1() {
        final int COUNTS = 10000000;
        List<Integer> linkedList = new LinkedList<>();
        List<Integer> arrayList = new ArrayList<>();
        Timer timerLinkedList = new Timer();
        Timer timerArrayList = new Timer();

        System.out.println("------------------");
        System.out.println("COUNTS: " + COUNTS);
        System.out.println("------------------");
        // ADD
        System.out.println("------------------");
        System.out.println("Add standard way");
        System.out.println("------------------");
        timerLinkedList.start();
        for (int i = 0; i < COUNTS; i++) {
            linkedList.add(Utility.getRandomNumber(0, 100));
        }
        timerLinkedList.stop();
        System.out.println("LinkedList -> add: " + timerLinkedList.getExecutionTime() + "ms");

        timerArrayList.start();
        for (int i = 0; i < COUNTS; i++) {
            arrayList.add(Utility.getRandomNumber(0, 100));
        }
        timerArrayList.stop();
        System.out.println("ArrayList -> add: " + timerArrayList.getExecutionTime() + "ms");
        System.out.println("------------------");
        System.out.println("Add index: COUNTS/2");
        System.out.println("------------------");
        timerLinkedList.start();
        linkedList.add(COUNTS / 2, Utility.getRandomNumber(0, 100));
        timerLinkedList.stop();
        System.out.println("LinkedList -> add(COUNTS/2): " + timerLinkedList.getExecutionTime() + "ms");
        timerArrayList.start();
        arrayList.add(COUNTS / 2, Utility.getRandomNumber(0, 100));
        timerArrayList.stop();
        System.out.println("ArrayList -> add(COUNTS/2): " + timerArrayList.getExecutionTime() + "ms");

        System.out.println("------------------");
        System.out.println("Add index: 0");
        System.out.println("------------------");
        timerLinkedList.start();
        linkedList.add(0, Utility.getRandomNumber(0, 100));
        timerLinkedList.stop();
        System.out.println("LinkedList -> add(0): " + timerLinkedList.getExecutionTime() + "ms");
        timerArrayList.start();
        arrayList.add(0, Utility.getRandomNumber(0, 100));
        timerArrayList.stop();
        System.out.println("ArrayList -> add(0): " + timerArrayList.getExecutionTime() + "ms");

        System.out.println("------------------");
        System.out.println("Get index: 0");
        System.out.println("------------------");
        timerLinkedList.start();
        linkedList.get(0);
        timerLinkedList.stop();
        System.out.println("LinkedList -> get(0): " + timerLinkedList.getExecutionTime() + "ms");
        timerArrayList.start();
        arrayList.get(0);
        timerArrayList.stop();
        System.out.println("ArrayList -> get(0): " + timerArrayList.getExecutionTime() + "ms");

        System.out.println("------------------");
        System.out.println("Get index: COUNTS/2");
        System.out.println("------------------");
        timerLinkedList.start();
        linkedList.get(COUNTS / 2);
        timerLinkedList.stop();
        System.out.println("LinkedList -> get(COUNTS/2): " + timerLinkedList.getExecutionTime() + "ms");
        timerArrayList.start();
        arrayList.get(COUNTS / 2);
        timerArrayList.stop();
        System.out.println("ArrayList -> get(COUNTS/2): " + timerArrayList.getExecutionTime() + "ms");

        System.out.println("------------------");
        System.out.println("Get index: LAST");
        System.out.println("------------------");
        timerLinkedList.start();
        linkedList.get(COUNTS - 1);
        timerLinkedList.stop();
        System.out.println("LinkedList -> get(COUNTS-1): " + timerLinkedList.getExecutionTime() + "ms");
        timerArrayList.start();
        arrayList.get(COUNTS - 1);
        timerArrayList.stop();
        System.out.println("ArrayList -> get(COUNTS-1): " + timerArrayList.getExecutionTime() + "ms");

        System.out.println("------------------");
        System.out.println("Remove index: 0");
        System.out.println("------------------");
        timerLinkedList.start();
        linkedList.remove(0);
        timerLinkedList.stop();
        System.out.println("LinkedList -> remove(0): " + timerLinkedList.getExecutionTime() + "ms");
        timerArrayList.start();
        arrayList.remove(0);
        timerArrayList.stop();
        System.out.println("ArrayList -> remove(0): " + timerArrayList.getExecutionTime() + "ms");

        System.out.println("------------------");
        System.out.println("Remove index: COUNTS/2");
        System.out.println("------------------");
        timerLinkedList.start();
        linkedList.remove(COUNTS / 2);
        timerLinkedList.stop();
        System.out.println("LinkedList -> remove(COUNTS/2): " + timerLinkedList.getExecutionTime() + "ms");
        timerArrayList.start();
        arrayList.remove(COUNTS / 2);
        timerArrayList.stop();
        System.out.println("ArrayList -> remove(COUNTS/2): " + timerArrayList.getExecutionTime() + "ms");

        System.out.println("------------------");
        System.out.println("Remove index: LAST");
        System.out.println("------------------");
        timerLinkedList.start();
        linkedList.remove(COUNTS - 1);
        timerLinkedList.stop();
        System.out.println("LinkedList -> remove(COUNTS-1): " + timerLinkedList.getExecutionTime() + "ms");
        timerArrayList.start();
        arrayList.remove(COUNTS - 1);
        timerArrayList.stop();
        System.out.println("ArrayList -> remove(COUNTS-1): " + timerArrayList.getExecutionTime() + "ms");

    }

    public static void task2() {
        System.out.println("------------------");
        System.out.println("FIFO");
        System.out.println("------------------");
        final int COUNTS = 16;
        Queue<Integer> queue = new LinkedList<>();
        for (int i = 0; i < COUNTS; i++) {
            queue.add(i);
        }
        for (int i = 0; i < COUNTS; i++) {
            System.out.println("Item: " + queue.poll());
        }
    }

    public static void task3() {
        System.out.println("------------------");
        System.out.println("LIFO");
        System.out.println("------------------");
        final int COUNTS = 16;
        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < COUNTS; i++) {
            stack.push(i);
        }
        for (int i = 0; i < COUNTS; i++) {
            System.out.println("Item: " + stack.pop());
        }
    }
    
    public static void task4() {
        System.out.println("------------------");
        System.out.println("PRIORITY QUEUE");
        System.out.println("------------------");
        final int COUNTS = 16;
        Queue<Person> priorityQueue = new PriorityQueue<>();
        for (int i = 0; i < COUNTS; i++) {
            priorityQueue.add(new Person.Builder()
                    .age(Utility.getRandomNumber(15, 80))
                    .name("Name" + i)
                    .address(new Address.Builder()
                            .city("Pardubice")
                            .postCode(53002)
                            .build())
                    .build());
        }
        for (int i = 0; i < COUNTS; i++) {
            System.out.println("Item: " + priorityQueue.poll());
        }
    }

}
