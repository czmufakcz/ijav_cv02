# Zadání úkolů

1. Změřte časové vykonání metody: add, get a remove u kolekce typu seznamu.
    + Vytvořte 2 kolekce typu seznam(List), který budou ukládat objekt Integer. Dodržte programování proti rozhraní.
    + První kolekce bude implementovat ArrayList a druhá kolekce LinkedList.
    + Nadále vygenerujte 1M, 5M, 10M objektů typu Integer a vložte postupně přes metodu add do 1 kolekce. Změřte čas vkládání.
    + To samé aplikujte pro druhou kolekci.
    + Nadále vyzkoušejte vložení na index v polovině pole (COUNTS/2) a změřte čas. V neposlední řadě aplikujte pro index 0. Pro obě kolekce.
    + Zkuste přístup k prvku přes index 0, v polovině pole a na konci pole. Pro každé aplikování změřte znovu čas. Pro obě kolekce.
    + Zopakujte předchozí bod pro metodu remove.
2. Vložte 10 prvků typu int do kolekce fronta s implementací LinkedList. (FIFO)
    + Prvky vložte od 0 do 15.
    + Prvky postupně odeberte a vypište.
    + Podívejte se do rozhraní Queue a použijte tomu odpovídající metodu!!!
3. Aplikujte bod 2 pro Stack.(LIFO)
4. Vložte 10 prvků typu Person do kolekce typu Queue s implementací PriorityQueue
    + Priorita bude podle věku. ASC. Nejmladší budou na začátku fronty.
    + Vypíšete první objekt ve frontě. 
    + Prvky postupně odeberte a vypište.
    + Podívejte se do rozhraní Queue a použijte tomu odpovídající metodu!!!
